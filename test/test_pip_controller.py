from unittest import TestCase

from pekli.pip_controller import PipInstaller


class TestPipInstalled(TestCase):
    def test_if_pip_install_click_then_it_is_available(self):
        with PipInstaller(["click"]):
            pass

    def test_if_pip_install_click_with_specific_version_then_it_is_available(self):
        with PipInstaller(["click==8.0.0"]):
            pass

    def test_if_pip_install_click_and_tomli_then_both_are_available(self):
        with PipInstaller(["tomli", "click"]):
            pass

    def test_if_empty_list_as_input_then_no_exceptions_are_raised(self):
        with PipInstaller([]):
            pass
