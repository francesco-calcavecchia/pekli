from pathlib import Path

import cloudpickle

from pekli import dump
from pekli.pip_controller import PipInstaller

cloudpickle_obj_path = Path(__file__).parent / Path("pathspec-pattern.cloudpickle")
cloudpickle_obj_requirements = ["pathspec==0.9.0"]
dynamopickle_obj_path = Path(__file__).parent / Path("mypyextensions-namedarg.pekli")
dynamopickle_obj_requirements = ["mypy_extensions==0.4.3"]


def generate_cloudpickle_file() -> None:
    with PipInstaller(cloudpickle_obj_requirements):
        from pathspec.pattern import Pattern

        a = Pattern(False)
        with open(cloudpickle_obj_path, "wb") as f:
            cloudpickle.dump(obj=a, file=f)


def generate_pekli_file() -> None:
    with PipInstaller(dynamopickle_obj_requirements):
        from mypy_extensions import NamedArg

        a = NamedArg
        dump(
            obj=a,
            path=dynamopickle_obj_path,
            requirements=dynamopickle_obj_requirements,
        )


if __name__ == "__main__":
    generate_cloudpickle_file()
    generate_pekli_file()
