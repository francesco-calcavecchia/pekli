from pathlib import Path
from unittest import TestCase

import cloudpickle

from pekli import dump, load, read_requirements
from pekli.pip_controller import PipInstaller
from test.data.generate_data import (
    cloudpickle_obj_path,
    dynamopickle_obj_path,
    dynamopickle_obj_requirements,
)


class TestPickle(TestCase):
    def setUp(self) -> None:
        self.tmp_file_path = Path(__file__).parent / Path("file.tmp")

    def tearDown(self) -> None:
        self.tmp_file_path.unlink() if self.tmp_file_path.is_file() else None

    def test_if_dumped_list_is_loaded_then_object_is_as_the_original(self):
        obj = [1, 2, 3]
        dump(obj=obj, path=self.tmp_file_path)
        loaded_obj = load(path=self.tmp_file_path)
        self.assertCountEqual(obj, loaded_obj)

    def test_if_read_requirements_from_cloudpickle_with_met_requirements_then_raise_attribute_error(
        self,
    ):
        with open(self.tmp_file_path, "wb") as f:
            cloudpickle.dump([1, 2, 3], f)
        with self.assertRaises(AttributeError):
            read_requirements(path=self.tmp_file_path)

    def test_if_read_requirements_from_cloudpickle_with_unmet_requirements_then_raise_module_not_found_error(
        self,
    ):
        with self.assertRaises(ModuleNotFoundError):
            read_requirements(path=cloudpickle_obj_path)

    def test_if_read_requirements_from_dynamopickle_then_are_returned_correctly(self):
        requirements = read_requirements(path=dynamopickle_obj_path)
        self.assertSequenceEqual(requirements, dynamopickle_obj_requirements)

    def test_if_load_regular_pickle_without_necessary_installed_packages_then_raise_error(
        self,
    ):
        with self.assertRaises(ModuleNotFoundError):
            load(path=cloudpickle_obj_path)

    def test_if_load_pekli_then_required_packages_are_installed(self):
        load(path=dynamopickle_obj_path)
        PipInstaller(
            requirements=read_requirements(path=dynamopickle_obj_path)
        ).__exit__(None, None, None)
