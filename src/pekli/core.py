from pathlib import Path
from typing import Optional, List

import cloudpickle

from pekli.wrapper import WrappedObject


def dump(obj: object, path: Path, requirements: Optional[List[str]] = None) -> None:
    wrapped_obj = WrappedObject(obj=obj, requirements=requirements)
    with open(path, mode="wb") as f:
        cloudpickle.dump(obj=wrapped_obj, file=f)


def load(path: Path) -> object:
    with open(path, mode="rb") as f:
        unpickled_obj = cloudpickle.load(file=f)
    if isinstance(unpickled_obj, WrappedObject):
        return unpickled_obj.unwrap()
    else:
        return unpickled_obj


def read_requirements(path: Path) -> List[str]:
    with open(path, mode="rb") as f:
        unpickled_obj = cloudpickle.load(file=f)
    return unpickled_obj.requirements
