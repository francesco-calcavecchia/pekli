import subprocess
from typing import List, Optional


class PipInstaller:
    def __init__(
        self, requirements: List[str], pip_options: Optional[List[str]] = None
    ) -> None:
        self._requirements = requirements
        self._pip_command_list = ["python", "-m", "pip"]
        self._pip_install_command_list = self._pip_command_list + ["install"]
        self._pip_uninstall_command_list = self._pip_command_list + ["uninstall", "-y"]
        if pip_options is not None:
            self._pip_install_command_list += pip_options
            self._pip_uninstall_command_list += pip_options

    def __enter__(self) -> None:
        subprocess.run(self._pip_install_command_list + self._requirements)

    def __exit__(self, exc_type, exc_value, tb) -> None:
        subprocess.run(self._pip_uninstall_command_list + self._requirements)

    def do(self):
        self.__enter__()
