from typing import Optional, List

import cloudpickle

from pekli.pip_controller import PipInstaller


class WrappedObject:
    def __init__(self, obj: object, requirements: Optional[List[str]]):
        self.cloudpickled_obj = cloudpickle.dumps(obj)
        self.requirements = list() if requirements is None else requirements

    def unwrap(self) -> object:
        PipInstaller(requirements=self.requirements).do()
        return cloudpickle.loads(self.cloudpickled_obj)
