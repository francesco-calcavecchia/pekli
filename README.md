# Pekli

Pekli means pickle in [esperanto](https://en.wikipedia.org/wiki/Esperanto).

The idea of this library is to include the requirements necessary to use a object that one wants to serialize.
In this way, to load a serialized object, it will not be necessary to
1. ask what requirements are needed
2. install them manually

In this way a pekli file will act as a living piece of code that can be dynamically loaded and used, without any 
added burden.



# Quickstart

Suppose that you have built a ML model that uses the scikit-learn and pandas library.
You can dump it in this way.
```python
from pekli import dump
from pathlib import Path

dump(obj=model,
     path=Path("model.pekli"),
     requirements=["scikit-learn==0.24.1", "pandas==1.2.3"])
```

You can then send the pekli file to a friend, without telling him anything about the environment you used to create 
that model. He will be able to load it from a clean virtual environment (scikit-learn and pandas are not 
installed):
```shell
> pip list
Package    Version
---------- -------
pip        21.2.4
setuptools 58.1.0
wheel      0.37.1

> python

>>> from pekli import load

>>> from pathlib import Path

>>> model = load(Path("model.pekli"))

>>> model.predict(...)

>>> exit()
````
